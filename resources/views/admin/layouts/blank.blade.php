<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <title>Alex</title>

        <!-- Bootstrap -->
        <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">
        <!-- Custom Theme Style -->
        <link href="{{ asset("css/gentelella.min.css") }}" rel="stylesheet">
        <!-- iCheck -->
        <link href="{{ asset("css/green.css") }}" rel="stylesheet">
        <!-- Switcherry -->
        <link href="{{ asset("css/switchery.min.css") }}" rel="stylesheet">
        
        <link href="{{ asset("css/admin.css") }}" rel="stylesheet">
        
        @stack('stylesheets')

    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">

                @include('admin/includes/sidebar')

                @include('admin/includes/topbar')

                @yield('main_container')

            </div>
        </div>

        <!-- jQuery -->
        <script src="{{ asset("js/jquery.min.js") }}"></script>
        <!-- Bootstrap -->
        <script src="{{ asset("js/bootstrap.min.js") }}"></script>
        <!-- Custom Theme Scripts -->
        <script src="{{ asset("js/gentelella.min.js") }}"></script>
        <!-- iCheck -->
        <script src="{{ asset("js/icheck.min.js") }}"></script>
        <!-- Switchery -->
        <script src="{{ asset("js/switchery.min.js") }}"></script>
        
        <script src="{{ asset("js/alex.js") }}"></script>
        @stack('scripts')
        
    </body>
</html>