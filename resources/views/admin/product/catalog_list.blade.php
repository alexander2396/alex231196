
<ul class="catalog-subtree" id="subtree-catalog-@if(isset($catalog->id)){{$catalog->id}}@endif" @if(isset($this_catalog) && isset($catalog) && $this_catalog->id == $catalog->id)style="display:block"@endif> 
    @foreach($catalogs as $catalog)
    <li id="catalog-{{ $catalog->id }}" class="with-subtree">
        <div class="list-container"> 
            <a href="/admin/product/catalog/{{ $catalog->id }}">{{ $catalog->title }}</a><div class="show-submenu">
                @if($catalog->ProductCatalog->count() > 0)
                    <span class="fa fa-chevron-down"></span>
                @endif
            </div>
        </div>
        @if($catalog->ProductCatalog->count() > 0)
            @include('admin.product.catalog_list', ['catalogs' => $catalog->ProductCatalog])
        @endif
    </li>
    @endforeach
</ul>