@extends('admin.layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">"-->
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">

        <div class="col-md-3 col-sm-3 col-xs-3">
            <div class="x_panel" style="background:rgba(52,73,94,1)">
                <div class="x_title" style="background:rgba(52,73,94,1)">
                    <h2 class="left"><a href="/admin/product" style="color:#fff">Каталоги</a></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content main_menu_side hidden-print main_menu">
                    @include('admin.product.catalog_list')
                </div>
            </div>   
        </div>
        
        <div class="col-md-9 col-sm-9 col-xs-9">
            <div class="x_panel">
                <div class="x_title">
                    <h1 class="left">{{ $page_title }}</h1>
                    <a href="/admin/product/create/0"><button type="button" class="btn btn-primary left" style="margin:13px 0 0 20px">Создать</button></a>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if (Session::has('flash message'))
                        <div class="alert alert-success">{{ Session::get('flash message') }}</div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead> 
                                <tr class="headings">
                                    <th>
                                        <div class="icheckbox_flat-green" ><input type="checkbox" name="hobbies[]" id="hobby1" value="ski" data-parsley-mincheck="2" required="" class="flat" data-parsley-multiple="hobbies" style="position: absolute; opacity: 0;"><ins class="iCheck-helper"></ins></div>
                                    </th>
                                    <th class="column-title">Название </th>
                                    <th class="column-title">Количество </th>
                                    <th class="column-title">Цена </th>
                                    <th class="column-title">Каталог </th>
                                    <th>Действия </th>
                                    <th class="column-title text-center">Активность </th>
                                </tr>
                            </thead>
                            <tbody> 
                                @foreach($products as $product)
                                    <tr class="odd pointer product-row-{{ $product->id }} @if($product->is_active == 0) inactive @endif">
                                        <td class="a-center ">
                                          <div class="icheckbox_flat-green" style="margin-top:3px"><input type="checkbox" name="hobbies[]" id="hobby1" value="ski" data-parsley-mincheck="2" required="" class="flat" data-parsley-multiple="hobbies" style="position: absolute; opacity: 0;"><ins class="iCheck-helper"></ins></div>
                                        </td>
                                        <td class=""><a href="/admin/product/{{ $product->id }}"><span>{!! $product->title !!}</span></a></td>
                                        <td class=""><span>{!! $product->main_combination->qtty !!}</span></td>
                                        <td class=""><span>{!! $product->main_combination->price !!}</span></td>
                                        <td class=""><span>{!! $product->catalog_ids !!}</span></td>            
                                        <td>
                                            <a href="/admin/product/update/{{ $product->id }}" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Редактировать </a>
                                            <a href="/admin/product/delete/{{ $product->id }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Удалить </a>
                                        </td>
                                        <td class="text-center"> 
                                            <div class="product-activity">  
                                                <input data-id="{{ $product->id }}" type="checkbox" class="js-switch" @if($product->is_active) checked="true" @endif data-switchery="true" style="display: none;">
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>			
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

    <!-- footer content -->
    <footer>
        <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
        </div>
        <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
@endsection