@extends('admin.layouts.blank')

@push('stylesheets')
    <!-- Example -->
    <!--<link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">"-->
@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">

        <div class="col-md-3 col-sm-3 col-xs-3">
            <div class="x_panel" style="background:rgba(52,73,94,1)">
                <div class="x_title" style="background:rgba(52,73,94,1)">
                    <h2 class="left"><a href="/admin/catalog" style="color:#fff">Каталоги</a></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content main_menu_side hidden-print main_menu">
                    @include('admin.catalog.catalog_list')
                </div>
            </div>   
        </div>
        
        <div class="col-md-9 col-sm-9 col-xs-9">
            <div class="x_panel">
                <div class="x_title">
                    <h1 class="left">{{ $page_title }}</h1>
                    <a href="/admin/catalog/create/{{ isset($this_catalog->id) ? $this_catalog->id : 0 }}"><button type="button" class="btn btn-primary left" style="margin:13px 0 0 20px">Добавить каталог</button></a>
                    @if(isset($this_catalog->id))<a href="/admin/product/catalog/{{ $this_catalog->id }}/add_product"><button type="button" class="btn btn-primary left" style="margin:13px 0 0 20px">Добавить товар</button></a>@endif
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if (Session::has('flash message'))
                        <div class="alert alert-success">{{ Session::get('flash message') }}</div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead> 
                                <tr class="headings">
                                    <th>
                                        <div class="icheckbox_flat-green" ><input type="checkbox" name="hobbies[]" id="hobby1" value="ski" data-parsley-mincheck="2" required="" class="flat" data-parsley-multiple="hobbies" style="position: absolute; opacity: 0;"><ins class="iCheck-helper"></ins></div>
                                    </th>
                                    <th class="column-title">Название </th>
                                    <th class="column-title">Дата создания </th>
                                    <th>Действия </th>
                                    <th class="column-title text-center">Активность </th>
                                </tr>
                            </thead>
                            <tbody> 
                                @foreach($catalogs_list as $catalog)
                                    <tr class="odd pointer catalog-row-{{ $catalog->id }} @if($catalog->is_active == 0) inactive @endif">
                                        <td class="a-center ">
                                          <div class="icheckbox_flat-green" style="margin-top:3px"><input type="checkbox" name="hobbies[]" id="hobby1" value="ski" data-parsley-mincheck="2" required="" class="flat" data-parsley-multiple="hobbies" style="position: absolute; opacity: 0;"><ins class="iCheck-helper"></ins></div>
                                        </td>
                                        <td class=""><a href="/admin/catalog/{{ $catalog->id }}"><span>{!! $catalog->title !!}</span></a></td>
                                        <td class=""><span>{!! $catalog->created_at !!}</span></td>            
                                        <td>
                                            <a href="/admin/catalog/update/{{ $catalog->id }}" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i> Редактировать </a>
                                            <a href="/admin/catalog/delete/{{ $catalog->id }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Удалить </a>
                                        </td>
                                        <td class="text-center"> 
                                            <div class="catalog-activity">  
                                                <input data-id="{{ $catalog->id }}" type="checkbox" class="js-switch" @if($catalog->is_active) checked="true" @endif data-switchery="true" style="display: none;">
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>			
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

    <!-- footer content -->
    <footer>
        <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
        </div>
        <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
@endsection