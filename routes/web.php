<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');

//main page
Route::get('/', 'IndexController@index');

//admin main
Route::get('/admin', 'Admin\IndexController@index');

//admin catalog
Route::get('/admin/catalog', 'Admin\CatalogController@index');
Route::get('/admin/catalog/{id}', 'Admin\CatalogController@catalog')->where('id', '[0-9]+');
Route::get('/admin/catalog/create/{id}', 'Admin\CatalogController@create')->where('id', '[0-9]+');
Route::post('/admin/catalog/create/{id}', 'Admin\CatalogController@create')->where('id', '[0-9]+');
Route::get('/admin/catalog/update/{id}', 'Admin\CatalogController@update')->where('id', '[0-9]+');
Route::post('/admin/catalog/update/{id}', 'Admin\CatalogController@update')->where('id', '[0-9]+');
Route::get('/admin/catalog/delete/{id}', 'Admin\CatalogController@delete')->where('id', '[0-9]+');

//admin product
Route::get('/admin/product', 'Admin\ProductController@index');
Route::get('/admin/product/create/{id}', 'Admin\ProductController@create')->where('id', '[0-9]+');
Route::post('/admin/product/create/{id}', 'Admin\ProductController@create')->where('id', '[0-9]+');
Route::get('/admin/product/update/{id}', 'Admin\ProductController@update')->where('id', '[0-9]+');
Route::post('/admin/product/update/{id}', 'Admin\ProductController@update')->where('id', '[0-9]+');
Route::get('/admin/product/delete/{id}', 'Admin\ProductController@delete')->where('id', '[0-9]+');
Route::get('/admin/product/catalog/{id}', 'Admin\ProductController@catalog')->where('id', '[0-9]+');

//ajax
Route::post('/ajax/catalog/activity', 'Ajax\CatalogController@activity_change');
Route::post('/ajax/product/activity', 'Ajax\ProductController@activity_change');
