<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Catalog;

class CatalogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
            $this->middleware('auth');
    }
    
    /**
     * Show catalogs index
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Catalog $catalogModel, Request $request)
    {
            
            //subsidiaries catalogs
            $catalogs_list = Catalog::where('parent_id', '=', 0)->get();

            //all catalogs
            $catalogs = $catalogModel->rootCatalog();

            return view('admin.catalog.index', [
                        'catalogs'   => $catalogs,
                        'catalogs_list'   => $catalogs_list,
                        'page_title' => 'Каталоги'
                    ]);
            
    }
    
    /**
     * Show catalog list
     *
     * @return \Illuminate\Http\Response
     */
    public function catalog(Catalog $catalogModel, Request $request, $id)
    {
            //this catalog
            $catalog = Catalog::find($id);
            
            //subsidiaries catalogs
            $catalogs_list = Catalog::where('parent_id', '=', $id)->get();
            
            //all catalogs
            $catalogs = $catalogModel->rootCatalog();

            return view('admin.catalog.index', [
                        'this_catalog'  => $catalog, 
                        'catalogs'      => $catalogs, 
                        'catalogs_list' => $catalogs_list,
                        'page_title'    => $catalog->title
                    ]);
            
    }
    
    /**
     * Show catalog create form
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Catalog $catalogModel, Request $request, $id)
    {
        
            if ($request->isMethod('post')) {
                
                $this->validate($request, [
                        'title' => 'required|max:255'
                ]);
                
                $data = $request->all();

                if($id > 0)
                {
                        $catalog = Catalogs::find($id);
                        $level = $catalog->level + 1;
                }
                else
                {
                        $level = 0;
                }
                
                Catalog::create([
                        'parent_id'     => $id,
                        'level'         => $level,
			'title'         => $data['title'],
			'description'   => $data['description'],
                        'slug'           => str_slug($data['title'])
		]);
                
                \Session::flash('flash message', 'Каталог создан');
        
                return $id > 0 ? redirect('admin/catalog/'.$id) : redirect('admin/catalog');

                    
            }
            
            $catalogs = Catalog::all();
           
            return view('admin.catalog.create', ['catalogs' => $catalogs]);
        
    }
    
     /**
     * Show catalog update form
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Catalog $catalogModel, Request $request, $id)
    {
        
            if ($request->isMethod('post')) {
                
                $this->validate($request, [
                        'title' => 'required|max:255'
                ]);
                
                $data = $request->all();
                
                $catalog = Catalog::find($id);
                $catalog->fill($data)->save();
                
                \Session::flash('flash message', 'Каталог изменен');
                
                return $catalog->parent_id > 0 ? redirect('admin/catalog/'.$catalog->parent_id) : redirect('admin/catalog');
     
            }
            
            $catalog = Catalog::find($id);
           
            return view('admin.catalog.update', ['catalog' => $catalog]);
        
    }
    
    /**
     * Delete catalog
     */
    public function delete(Catalog $catalogModel, Request $request, $id)
    {
  
        if($id > 0)
        {
                $catalog = Catalog::find($id);
                $catalog->delete();
                
                \Session::flash('flash message', 'Каталог удален');
                
                return redirect()->back();
        }
        
    }
}