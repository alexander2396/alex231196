<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Combination;
use App\Models\Catalog;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
            $this->middleware('auth');
    }
    
    /**
     * Show products list
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $productModel, Catalog $catalogModel, Request $request)
    {
        
            $catalogs = $catalogModel->rootCatalog();
            $products = $productModel->with('main_combination')->with('catalogs')->get();
            
            foreach($products as $product)
            {
                    foreach($product->catalogs as $catalog)
                    {
                            //$product->catalog_ids = $product->catalog_ids == NULL ? $product->catalog_ids.$catalog->title : $product->catalog_ids.', '.$catalog->title;
                            $product->catalog_ids = $catalog->title;
                    }
            }
            
            
            return view('admin.product.index', [
                        'catalogs'   => $catalogs,
                        'products'   => $products,
                        'page_title' => 'Товары'
                    ]);
            
    }
    
    /**
     * Show products list in catalog
     *
     * @return \Illuminate\Http\Response
     */
    public function catalog(Product $productModel, Catalog $catalogModel, Request $request, $id)
    {
        
            $catalogs = $catalogModel->rootCatalog();
            
            $catalog = $catalogModel->find($id);
            $products = \DB::table('products')
                ->join('product_catalog', 'products.id', '=', 'product_catalog.product_id')
                ->join('catalogs', 'product_catalog.catalog_id', '=', 'catalogs.id')
                ->join('combinations', 'products.main_combination_id', '=', 'combinations.id')
                ->select(
                        'products.id',
                        'products.title',
                        'products.description',
                        'products.is_active',
                        'combinations.qtty',
                        'combinations.price',
                        'products.created_at',
                        'products.updated_at'
                        )
                ->where('catalogs.id', '=', $id)
                ->get();
            
            return view('admin.product.catalog', [
                        'catalog'    => $catalog,
                        'catalogs'   => $catalogs,
                        'products'   => $products,
                        'page_title' => $catalog->title.'/Товары'
                    ]);
            
    }
    
     /**
     * Show product create form
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Product $productModel, Request $request, $id)
    {

            if ($request->isMethod('post')) {     
                
                $data = $request->all();      
                
                //save main conbination
                $combination = Combination::create([
			'qtty'            => $data['qtty'],
                        'price'           => str_slug($data['price'])
		]);
                
                //save product 
                $product = Product::create([
                        'title'                 => $data['title'],
                        'description'           => $data['description'],
			'main_combination_id'   => $combination->id,
		]);
                
                if($id > 0)
                {
                        \DB::table('product_catalog')->insert(array('product_id' => $product->id, 'catalog_id' => $id));
                }
                
                //set main_combination product_id
                $combination->product_id = $product->id;
                $combination->save();
                
                \Session::flash('flash message', 'Товар создан');
                
                return $id > 0 ? redirect('admin/product/catalog/'.$id) : redirect('admin/product');
   
            }
            
            $products = Product::all();
           
            return view('admin.product.create', ['products'   => $products]);
        
    }
    
     /**
     * Show product update form
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Product $productModel, Request $request, $id)
    {
        
            if ($request->isMethod('post')) {
                
                $this->validate($request, [
                        'title' => 'required|max:255'
                ]);
                
                $data = $request->all();
                
                $product = Product::find($id);
                $product->fill($data)->save();
                
                $combination = Combination::find($product->main_combination_id);
                $combination->qtty = $data['qtty'];
                $combination->price = $data['price'];
                $combination->save();
                
                \Session::flash('flash message', 'Товар изменен');
                
                return redirect('admin/product');
     
            }
            
            $product = $productModel->with('main_combination')->find($id);
         
            return view('admin.product.update', ['product' => $product]);
        
    }
    
     /**
     * Delete product
     */
    public function delete(Product $productModel, Request $request, $id)
    {
  
        if($id > 0)
        {
                $product = Product::find($id);
                $product->delete();
                
                $combination = Combination::find($product->main_combination_id);
                $combination->delete();
                
                \Session::flash('flash message', 'Товар удален');
                
                return redirect()->back();
        }
        
    }


}