<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Models\Product;

class ProductController extends Controller
{
    public function activity_change(){
       
            if(\Request::ajax()) {
                
                $data = \Request::all();
               
                $product = Product::find($data['id']);
         
                $product->is_active = $data['value'] == 'false' ? 0 : 1;
                $product->save(); 
              
                return response()->json([
                    'activity' => $product->is_active
                ]);
            }
            
    }
}