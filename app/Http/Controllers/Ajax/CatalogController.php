<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Models\Catalog;

class CatalogController extends Controller
{
    public function activity_change(){
       
            if(\Request::ajax()) {
                
                $data = \Request::all();
               
                $catalog = Catalog::find($data['id']);
         
                $catalog->is_active = $data['value'] == 'false' ? 0 : 1;
                $catalog->save(); 
              
                return response()->json([
                    'activity' => $catalog->is_active
                ]);
            }
            
    }
}