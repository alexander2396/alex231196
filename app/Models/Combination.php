<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Combination extends Model
{
    protected $fillable = ['product_id','qtty','price','created_at','updated_at'];
    
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
