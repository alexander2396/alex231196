<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    protected $fillable = ['parent_id','level','title','slug','description','is_active','created_at','updated_at'];
    
    public function ProductCatalog(){
        return $this->hasMany($this, 'parent_id')->where('is_active', '=', 1);
    }
    
    /*
     * Return catalog tree
     */
    public function rootCatalog(){
        return $this->where('parent_id', 0)->where('is_active', '=', 1)->with('ProductCatalog')->get();
    }
    
    /**
     * 
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product','product_catalog');
    }
}
