<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['title','slug','main_combination_id','description','is_active','created_at','updated_at'];
    
    public $catalog_ids;
    
    //главная комбинация товара
    public function main_combination()
    {
         return   $this->hasOne('App\Models\Combination');
    }
    
    /**
     * 
     */
    public function catalogs()
    {
        return $this->belongsToMany('App\Models\Catalog','product_catalog');
    }
    
}
