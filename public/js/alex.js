
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('.with-subtree .show-submenu').each(function(){  
    $(this).click(function(){ 
        var el = $(this).closest("li.with-subtree"); 
        var id = $(el).attr('id');
        $('#subtree-'+id).toggle("slow");
    });
});

$(document).ready(function(){
    
    $( ".catalog-activity" ).click(function() {       
        var id = $(this).find(".js-switch").attr('data-id');
        var value = $(this).find(".js-switch").prop('checked');
        $.post("/ajax/catalog/activity", {id: id,value: value}, function(data){
            if(data.activity == 0){
                $('.catalog-row-'+id).addClass('inactive');        
            }
            else
            {
                $('.catalog-row-'+id).removeClass('inactive');
            }
        });  
    });
  
    $( ".product-activity" ).click(function() {       
        var id = $(this).find(".js-switch").attr('data-id');
        var value = $(this).find(".js-switch").prop('checked');
        $.post("/ajax/product/activity", {id: id,value: value}, function(data){
            if(data.activity == 0){
                $('.product-row-'+id).addClass('inactive');        
            }
            else
            {
                $('.product-row-'+id).removeClass('inactive');
            }
        });  
    });
  
});